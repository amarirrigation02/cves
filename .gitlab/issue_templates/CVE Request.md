### CVE Identifier Request

**NOTE:** Only maintainers of GitLab-hosted projects may request a CVE for
a vulnerability within their project.

#### Publishing Schedule

After we check and approve your CVE request, our system will automatically
reserve a CVE identifier with MITRE.

Select *Publish immediately* If you want your advisory published as soon as the
CVE identifier is assigned.

Select *Wait to publish* if you want to wait to publish. You can switch this to
"Publish Immediately" whenever you want, to let let the system know it's ready
to be published.

* [ ] Publish immediately
* [x] Wait to publish

#### Advisory Details

Fill out the details about the advisory in the YAML code block below. This data
will be automatically validated and manually reviewed by a member of the
Vulnerability Research team.

If you're unsure about what to put in a field, leave it as "TODO" and we can
assist you with finding an appropriate CWE ID, CVSS score, or anything else you
might need.

```yaml
reporter:
  name: "TODO" # "First Last"
  email: "TODO" # "email@domain.tld" or omit if unknown
vulnerability:
  description: "TODO" # "[VULNTYPE] in [COMPONENT] in [VENDOR][PRODUCT] [VERSION] allows [ATTACKER] to [IMPACT] via [VECTOR]"
  cwe: "TODO" # "CWE-22" # Find an appropriate Common Weakness Enumeration (CWE) ID at https://cwe.mitre.org/index.html
  product:
    gitlab_path: "TODO" # "namespace/project" # the path of the project within gitlab
    vendor: "TODO" # "iTerm2"
    name: "TODO" # "iTerm2"
    affected_versions:
      - "TODO" # "1.2.3"
      - "TODO" # ">1.3.0, <=1.3.9"
    fixed_versions:
      - "TODO" # "1.2.4"
      - "TODO" # "1.3.10"
  impact: "TODO" # CVSS v3.1 Base Score vector generated with https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator
  solution: "TODO" # "Upgrade to version 1.2.4 or 1.3.10"
  credit: "TODO"
  references:
    - "TODO" # "https://some.domain.tld/a/reference"
```

CVSS scores can be computed by means of the [NVD CVSS Calculator](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator).

<!-- These quick actions assign correct labels and makes the issue confidential. Please do not edit or remove! -->
/label ~"devops::secure" ~"group::vulnerability research" ~"vulnerability research::cve" ~"advisory::queued" ~"advisory-group::external"
/confidential
